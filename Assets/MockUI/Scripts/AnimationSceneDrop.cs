using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSceneDrop : MonoBehaviour
{
    
    private AnimateMoveAnimation animateMoveAnimation;

    void Awake()
    {
       animateMoveAnimation = GetComponent<AnimateMoveAnimation>(); 
    }

    void Start()
    {
        animateMoveAnimation.dropAnimation();
    }

}
