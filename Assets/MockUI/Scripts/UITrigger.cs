public enum UITrigger
{
    EnterToMainMenu,
    EnterToLevelMenu,
    EnterToScoreMenu,
    EnterToSettingnMenu,
    EnterToLike,
    EnterToAchievement,
    EnterToGameplay,
    GoingToExit,
    
}