using Stateless;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateSettingMenuConfig : MonoBehaviour
{
    public void StateSettingMenuConfigSetup(StateMachine<UIState, UITrigger> state)
    {
        state.Configure(UIState.SettingMenu)
        .Permit(UITrigger.EnterToMainMenu, UIState.MainMenu)
        .Permit(UITrigger.EnterToGameplay, UIState.GamePlay)
            .OnEntry(
                () =>
                {
                    SceneManager.LoadSceneAsync("OptionMenuScene", LoadSceneMode.Additive);

                })
            .OnExit(
                () =>
                {

                    SceneManager.UnloadSceneAsync("OptionMenuScene");
                }
            );
    }
}
