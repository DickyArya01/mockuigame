using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AnimateBounceLoopAnimation : AnimationObjectHandler 
{
    private float duration;
    private float bounceScaleX;
    private float bounceScaleY;

    void Awake()
    {
       duration = Constant.LONG_DURATION; 
       bounceScaleX = Constant.SCALED_BOUNCE_X;
       bounceScaleY = Constant.SCALED_BOUNCE_Y;

 
    }

    void Start()
    {
       bounceAnimationLoop(); 
    }

    public void bounceAnimationLoop()
    {
        GameObjectToAnimated.transform.DOScaleX(bounceScaleX, duration).SetEase(Ease.InOutBounce).SetLoops(-1, LoopType.Yoyo);
        GameObjectToAnimated.transform.DOScaleY(bounceScaleY, duration).SetEase(Ease.InOutBounce).SetLoops(-1, LoopType.Yoyo);

    }

}
