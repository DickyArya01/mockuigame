using System.Collections;
using System.Collections.Generic;
using Stateless;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateScoreMenuConfig : MonoBehaviour
{
    public void StateScoreMenuConfigSetup(StateMachine<UIState, UITrigger> state)
    {
        state.Configure(UIState.ScoreMenu)
        .Permit(UITrigger.EnterToMainMenu, UIState.MainMenu)
            .OnEntry(
                () =>
                {
                    SceneManager.LoadSceneAsync("ScoreMenuScene", LoadSceneMode.Additive);
                })
            .OnExit(
                () =>
                {
                    SceneManager.UnloadSceneAsync("ScoreMenuScene");
                });
    }
}
