using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class AnimateOnClickButton : AnimationObjectHandler 
{

    public UnityEvent unityEvent;

    public void OnButtonClicked()
    {
        GameObjectToAnimated.transform.DOScale(Constant.ONCLICK_SCALED_VALUE, Constant.SHORT_DURATION).OnComplete(()=>{
            GameObjectToAnimated.transform.DOScale(1f, Constant.SHORT_DURATION);
            unityEvent?.Invoke();
        });

    }
}
