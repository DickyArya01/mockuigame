using System.Collections;
using System.Collections.Generic;
using Stateless;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateMainMenuConfig : MonoBehaviour
{
    public void StateMainMenuConfigSetup(StateMachine<UIState, UITrigger> state)
    {
        state.Configure(UIState.MainMenu)
        .Permit(UITrigger.EnterToLevelMenu, UIState.LevelMenu)
        .Permit(UITrigger.EnterToScoreMenu, UIState.ScoreMenu)
        .Permit(UITrigger.EnterToSettingnMenu, UIState.SettingMenu)
        .Permit(UITrigger.GoingToExit, UIState.Exit)
            .OnEntry(
                () =>
                {
                    SceneManager.LoadSceneAsync("MainMenuScene", LoadSceneMode.Single);
                })
            .OnExit(
                () =>
                {
                    // SceneManager.UnloadSceneAsync("MainMenuScene");
                });
    }

}

