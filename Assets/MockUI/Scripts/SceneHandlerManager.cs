using DG.Tweening;
using Stateless;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;


public class SceneHandlerManager : StateConfig
{


    void Start()
    {
        StateCycle();
    }

    private void StateCycle()
    {
        InitiateMainMenuConfig();

        InitiateLevelMenuConfig();

        InitiateSettingConfig();

        InitiateScoreConfig();

        InitiateExitConfig();
    }

    public void ToSettingMenu()
    {
        GameUIState.Fire(UITrigger.EnterToSettingnMenu);
    }

    public void ToScoreMenu()
    {
        GameUIState.Fire(UITrigger.EnterToScoreMenu);
    }

    public void ToLevelMenu()
    {
        GameUIState.Fire(UITrigger.EnterToLevelMenu);
    }

    public void ExitGame()
    {
        GameUIState.Fire(UITrigger.GoingToExit);
    }

    public void ToMainMenu()
    {
        GameUIState.Fire(UITrigger.EnterToMainMenu);
    }




}
