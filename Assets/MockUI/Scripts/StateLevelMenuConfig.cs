using Stateless;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateLevelMenuConfig: MonoBehaviour
{
    public void StateLevelMenuConfigSetup(StateMachine<UIState, UITrigger> state)
    {
        state.Configure(UIState.LevelMenu)
            .Permit(UITrigger.EnterToMainMenu, UIState.MainMenu)
            .Permit(UITrigger.EnterToGameplay, UIState.GamePlay)
                .OnEntry(
                    () =>
                    {
                        Debug.Log(state.State);
                        SceneManager.LoadSceneAsync("LevelMenuScene", LoadSceneMode.Additive);
                    })
                .OnExit(
                    () =>
                    {
                        SceneManager.UnloadSceneAsync("LevelMenuScene");
                    });
    }

}
