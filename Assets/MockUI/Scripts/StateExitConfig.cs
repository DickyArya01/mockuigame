using Stateless;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateExitConfig: MonoBehaviour
{
    public void StateExitConfigSetup(StateMachine<UIState, UITrigger> state)
    {
        state.Configure(UIState.Exit)
        .Permit(UITrigger.EnterToMainMenu, UIState.MainMenu)
            .OnEntry(
                () =>
                {
                    SceneManager.LoadSceneAsync("ExitDialogScene", LoadSceneMode.Additive);

                })
            .OnExit(
                () =>
                {

                    SceneManager.UnloadSceneAsync("ExitDialogScene");
                }
            );
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
