using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationObjectHandler : MonoBehaviour
{
    public GameObject GameObjectToAnimated;
    public RectTransform rectTransform;

    void Awake()
    {
        GameObjectToAnimated = this.gameObject;
        rectTransform = GetComponent<RectTransform>();
    }


}
