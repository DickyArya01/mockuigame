using System;

public interface IUIState
{
    UIState StateSaved {get; set;}
}