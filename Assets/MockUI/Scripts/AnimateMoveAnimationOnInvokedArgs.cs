using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class AnimateMoveAnimationOnInvokedArgs : AnimationObjectHandler
{
    private float duration;
    private Vector3 moveToX;
    private Vector3 moveToY;
    public UnityEvent invokedArgs;

    void Awake()
    {
       duration = Constant.LONG_DURATION;
       moveToX = transform.right * Constant.PANEL_MOVETO;
       moveToY = transform.up * Constant.PANEL_MOVETO; 
    }

    public void flyAnimation(){
        rectTransform.DOLocalMoveY(3000, duration).SetEase(Ease.OutSine).onComplete += ()=> {
            invokedArgs.Invoke();
        };
    }

    public void slideBackAnimation()
    {
        rectTransform.DOLocalMoveX(3000, duration, false).SetEase(Ease.OutSine).onComplete += () => {
            invokedArgs.Invoke();
        };
    }
    
}
