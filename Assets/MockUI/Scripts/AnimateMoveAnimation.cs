using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AnimateMoveAnimation: AnimationObjectHandler 
{
    private float duration;
    private Vector3 moveToX;
    private Vector3 moveToY;


    void Awake()
    {
       duration = Constant.LONG_DURATION;
       moveToX = transform.right * Constant.PANEL_MOVETO;
       moveToY = transform.up * Constant.PANEL_MOVETO;
    }

    public void slideAnimation()
    {
        rectTransform.transform.localPosition = moveToX;
        rectTransform.DOAnchorPos(new Vector2(0f, 0f), duration, false).SetEase(Ease.InBack);
    }

    public void dropAnimation(){
        rectTransform.transform.localPosition = moveToY;
        rectTransform.DOAnchorPos(new Vector2(0f, 0f), duration, false).SetEase(Ease.OutElastic);
    }

}
