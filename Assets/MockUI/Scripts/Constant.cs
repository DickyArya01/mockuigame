public class Constant 
{
    public const float SHORT_DURATION = 0.2f;
    public const float LONG_DURATION = 0.4f;

    public const float ONCLICK_SCALED_VALUE = 0.8f;

    public const float SCALED_BOUNCE_X = 1.03f;
    public const float SCALED_BOUNCE_Y = 0.98f;

    public const float PANEL_MOVETO = 3000.0f;
}
