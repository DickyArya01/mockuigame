using System.Collections;
using System.Collections.Generic;
using Stateless;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateConfig : MonoBehaviour
{
    public StateMachine<UIState, UITrigger> GameUIState;
    public UIState CurrentState;

    public StateMainMenuConfig stateMainConfig;
    public StateLevelMenuConfig stateLevelConfig;
    public StateSettingMenuConfig stateSettingConfig;
    public StateScoreMenuConfig stateScoreMenuConfig;
    public StateExitConfig stateExitConfig;

    void Awake()
    {
        GameUIState = new StateMachine<UIState, UITrigger>(CurrentState);

        stateMainConfig = GetComponent<StateMainMenuConfig>();
        stateLevelConfig = GetComponent<StateLevelMenuConfig>();
        stateSettingConfig = GetComponent<StateSettingMenuConfig>();
        stateScoreMenuConfig = GetComponent<StateScoreMenuConfig>();
        stateExitConfig = GetComponent<StateExitConfig>();
    }



    public void InitiateMainMenuConfig()
    {
        stateMainConfig.StateMainMenuConfigSetup(GameUIState);
    }

    public void InitiateLevelMenuConfig()
    {
       stateLevelConfig.StateLevelMenuConfigSetup(GameUIState); 
    }

    public void InitiateSettingConfig()
    {
        stateSettingConfig.StateSettingMenuConfigSetup(GameUIState);
    }

    public void InitiateScoreConfig()
    {
        stateScoreMenuConfig.StateScoreMenuConfigSetup(GameUIState);
    }

    public void InitiateExitConfig()
    {
        stateExitConfig.StateExitConfigSetup(GameUIState);
    }

}
